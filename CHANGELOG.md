# v1.2.1

## Release v1.2.1 - 2024/05/10

### Improvements

* Added support for specifying URI for nodes to which the mqtt-http plugin sends HTTP messages.
  * Please refer to [README.md](README.md#register-mqtt-http-plugin) or [miraclelinux8.md](docs/miraclelinux8.md#mqtt-httpプラグインを登録する) for details

### Bug fixes

* Fixed a bug that this plugin couldn't work with manually built APISIX 3.4.1+.
  * The required libraries are old, we updated them.
    * ext-plugin-proto 0.5.0 -> 0.6.0
    * lua-resty-etcd 1.7.0 -> 1.10.4

# v1.2.0

## Release v1.2.0 - 2023/08/29

### Improvements

* Added support for APISIX 3
  * The rpm package was separated to a main package and config packages.
    We should install the main package and an appropriate config package.
    Please refer to [miraclelinux8.md](docs/miraclelinux8.md#APISIXプラグインのインストール) for details.
* Added support for QoS2 under MQTT version 5
  * We cannot use QoS2 under MQTT versions earlier than 5.0
* Improve performance for QoS1
* Enabled to specify properties when sending CONNECT and SUBSCRIBE
  * Please refer to [README.md](README.md#subscribe-to-mqtt-broker) or [miraclelinux8.md](docs/miraclelinux8.md#mqttブローカーに登録する) for details
* Added support for the [Server Keep Alive](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901094) property of CONNACK.

### Miscellaneous

* Updated the required APISIX version to 2.15.3+
* Modified the default admin port to 9180 from 9080
  * 9180 is the default admin port of APISIX 3, we adjusted it accordingly.

# v1.1.0

## Release v1.1.0 - 2022/07/25

### Improvements

* MQTTS with CA file was supported.
* User name and password parameters for subscriber were supported.

### Bug fixes

* Fixed to set timeout for receiving connack from a broker.
  It fixes a bug that inconsistent "Subscribed" message may returns.

### Miscellaneous

* doc: Updated explanation about usage with `user_name` and `password` parameters.
* doc: Added explanation about `plugin_attr.cafile` and `plugin_attr.tls_version` configurations.
