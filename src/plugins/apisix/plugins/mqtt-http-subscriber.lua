local core = require("apisix.core")
local profile = require("apisix.core.profile")
local ngx = ngx
local signal = require("posix.signal")
local unistd = require("posix.unistd")
local ngx_sleep = require("apisix.core.utils").sleep
local plugin = require("apisix.plugin")
local json = core.json
local util = require("apisix.cli.util")

local schema = {
    type = "object",
    properties = {
        broker_uri = {type = "string"},
        upstream_uri = {type = "string"},
    },
}

local plugin_name = "mqtt-http-subscriber"
local mqtt_subscriber_file_name = "relay-subscriber.lua"
local mqtt_subscriber_file_name_for_find = string.gsub(mqtt_subscriber_file_name, "-", "%%%1")
local subscriber_pid_duc_key_delimiter = "__"

local _M = {
    version = 0.1,
    priority = 1000,
    name = plugin_name,
    schema = schema,
}

function _M.check_schema(conf)
    return core.schema.check(schema, conf)
end

local function return_result(return_code, result_message)
    return return_code, result_message .. "\n"
end

local function is_subscriber_process_alive(pid)
    local handle = io.popen("ps -p " .. pid .. " -o args")
    local is_subscriber_process = false
    for line in handle:lines() do
        if string.find(line, mqtt_subscriber_file_name_for_find) then
            is_subscriber_process = true
        end
    end
    handle:close()
    return is_subscriber_process
end

local function get_request_table()
    ngx.req.read_body()
   
    local method = ngx.req.get_method()
    local content_type = ngx.req.get_headers()["Content-Type"] or ""
    if string.find(content_type, "application/json", 1, true) and
        (method == "POST" or method == "PUT" or method == "PATCH")
    then
        local req_body, _ = core.request.get_body()
        if req_body then
            local data, _ = json.decode(req_body)
            if data then
                return data
            end
        end
    end

    if method == "POST" then
        return ngx.req.get_post_args()
    end

    return ngx.req.get_uri_args()
end

local function create_subscriber_pid_dic_key(broker_uri, upstream_uri, topic)
    local delimiter = subscriber_pid_duc_key_delimiter
    return broker_uri .. delimiter .. upstream_uri .. delimiter .. topic
end

local function unsubscribe(pid)
    if not pid then
        -- Regard as it is already unsubscribed
        return true
    end

    if is_subscriber_process_alive(pid) then
        signal.kill(pid, signal.SIGTERM)
        ngx_sleep(1)
    else
        return true
    end
    
    if is_subscriber_process_alive(pid) then
        -- Not stopped by SIGTERM, stop force
        signal.kill(pid, signal.SIGKILL)
        ngx_sleep(1)
    else
        return true
    end

    if is_subscriber_process_alive(pid) then
        return false, 'Could not unsubscribe, please manually stop pid:' .. pid
    end
end

local function unsubscribe_handler()
    local request_table = get_request_table()

    local broker_uri = request_table.broker_uri or "localhost:1883"
    local upstream_uri = request_table.upstream_uri or "localhost:9100"
    local topic = request_table.topic or "/#"

    core.log.debug("broker_uri: " .. broker_uri)
    core.log.debug("upstream_uri: " .. upstream_uri)
    core.log.debug("topic: " .. topic)

    local key = create_subscriber_pid_dic_key(broker_uri, upstream_uri, topic)

    core.log.debug("dictionary key: " .. key)

    local subscriber_pid_dic = ngx.shared.mqtt_http_subscriber_pid_dic
    local pid = subscriber_pid_dic:get(key)

    local ok, why = unsubscribe(pid)

    if ok then
        subscriber_pid_dic:delete(key)
    end

    local return_code = ok and 200 or 500
    local return_message = ok and "Unsubscribed" or "Failed to unsubscribe:" .. why
    
    return return_result(return_code, return_message)
end

local function start_subscriber(
    broker_uri,
    upstream_uri,
    topic,
    version,
    user_name,
    password,
    connect_properties,
    subscribe_properties
)
    -- TODO: Get path from config and so on.
    local relay_subscriber_path = profile.apisix_home .. "plugins/tools/" .. mqtt_subscriber_file_name
    local attr = plugin.plugin_attr(plugin_name)
    local relay_subscriber_log_level = "warn"
    local relay_subscriber_log_max_file_size = "10485760"
    local relay_subscriber_log_max_backup_index = "10"
    local cafile = nil
    local tls_version = nil
    if attr then
        if attr.relay_subscriber_path then
            relay_subscriber_path = attr.relay_subscriber_path
            core.log.info("relay_subscriber_path is overridden: " .. relay_subscriber_path)
        end
        if attr.relay_subscriber_log_level then
            relay_subscriber_log_level = attr.relay_subscriber_log_level
            core.log.info("relay_subscriber_log_level is overridden: " .. relay_subscriber_log_level)
        end
        if attr.relay_subscriber_log_max_file_size then
            relay_subscriber_log_max_file_size = attr.relay_subscriber_log_max_file_size
            core.log.info("relay_subscriber_log_max_file_size is overridden: " .. relay_subscriber_log_max_file_size)
        end
        if attr.relay_subscriber_log_max_backup_index then
            relay_subscriber_log_max_backup_index = attr.relay_subscriber_log_max_backup_index
            core.log.info("relay_subscriber_log_max_backup_index is overridden: " .. relay_subscriber_log_max_backup_index)
        end
        if attr.cafile then
            cafile = attr.cafile
            core.log.info("cafile: " .. cafile)
        end
        if attr.tls_version then
            tls_version = attr.tls_version
            core.log.info("tls_version: " .. tls_version)
        end
    end

    local options = {
        "-b " .. broker_uri,
        "-U " .. upstream_uri,
        "-t " .. topic,
        "-v " .. version,
        "-p " .. unistd.getpid(),
        "-n openresty",
        "--log_level " .. relay_subscriber_log_level,
        "--log_max_file_size " .. relay_subscriber_log_max_file_size,
        "--log_max_backup_index " .. relay_subscriber_log_max_backup_index,
    }

    if user_name then
        table.insert(options, "-u " .. user_name)
    end

    if password then
        table.insert(options, "-P " .. password)
    end

    if cafile then
        table.insert(options, "--cafile " .. cafile)
    end

    if tls_version then
        table.insert(options, "--tls_version " .. tls_version)
    end

    if connect_properties then
        local value = json.encode(connect_properties)
        value = string.gsub(value,"'","'\\''")
        value = "'" .. value .. "'"
        table.insert(options,"--connect_properties " .. value)
    end

    if subscribe_properties then
        local value = json.encode(subscribe_properties)
        value = string.gsub(value,"'","'\\''")
        value = "'" .. value .. "'"
        table.insert(options,"--subscribe_properties " .. value)
    end

    local command_line = "luajit " .. relay_subscriber_path .. " " .. table.concat(options," ") .. " 2>&1 &"
    core.log.info("launch subscriber: " .. command_line)
    local handle = io.popen(command_line)
  
    ngx_sleep(1)

    local line = handle:read("*l")
    local pid = tonumber(line)

    if not pid then
        -- Failed to start command
        local err_head = line or ""
        local err_rest = handle:read("*a")
        reason_rest = reason_rest or ""

        local err = err_head .. "\n" .. err_rest
        core.log.error("Failed to start command:" .. err)
        handle:close()
        return nil, err
    end

    -- If failed to start subscription, process is terminated.
    if not is_subscriber_process_alive(pid) then
        local err = handle:read("*a") or ""
        core.log.error("Subscriber process is terminated:" .. err)
        handle:close()
        return nil, err
    end

    handle:close()
    return pid, nil
end 

local function subscribe_handler()
    local request_table = get_request_table()
    local broker_uri = request_table.broker_uri or "localhost:1883"
    local upstream_uri = request_table.upstream_uri or "localhost:9100"
    local topic = request_table.topic or "/#"
    local version = request_table.mqtt_version or "5"
    local user_name = request_table.user_name
    local password = request_table.password
    local connect_properties = request_table.connect_properties
    local subscribe_properties = request_table.subscribe_properties

    core.log.debug("broker_uri: " .. broker_uri)
    core.log.debug("upstream_uri: " .. upstream_uri)
    core.log.debug("topic: " .. topic)
    core.log.debug("version: " .. version)
    core.log.debug("user_name: " .. (user_name or ""))
    core.log.debug("password: " .. (password or ""))
    core.log.debug("connect_properties: " .. (json.encode(connect_properties) or ""))
    core.log.debug("subscribe_properties: " .. (json.encode(subscribe_properties) or ""))

    local key = create_subscriber_pid_dic_key(broker_uri, upstream_uri, topic)

    core.log.debug("dictionary key: " .. key)

    local subscriber_pid_dic = ngx.shared.mqtt_http_subscriber_pid_dic

    if subscriber_pid_dic:get(key) then
        return return_result(200, "Already subscribed")
    end

    local pid,err = start_subscriber(
        broker_uri,
        upstream_uri,
        topic,
        version,
        user_name,
        password,
        connect_properties,
        subscribe_properties
    )

    if not pid then
        core.log.error("Failed to start subscriber:" .. err)
        return return_result(500, "Failed to start subscriber:" .. err)
    end

    subscriber_pid_dic:safe_set(key, pid)
    return return_result(200, "Subscribed")
end

local function status_handler()
    local subscriber_pid_dic = ngx.shared.mqtt_http_subscriber_pid_dic
    local result_table = {}
    local keys = subscriber_pid_dic:get_keys(0)

    if not keys or #keys == 0 then
        return return_result(200, "No subscriber is running")
    end
    for _, key in ipairs(keys)  do
        local fields = util.split(key, subscriber_pid_duc_key_delimiter)
        local pid = subscriber_pid_dic:get(key)
        table.insert(result_table, {
            pid = pid,
            broker_uri = fields[1],
            upstream_uri = fields[2],
            topic = fields[3]
        })
    end
    return return_result(200, json.encode(result_table))
end

function _M.api()
    local base_uri = "/apisix/plugin/mqtt-http-subscriber"
    local attr = plugin.plugin_attr(plugin_name)
    if attr and attr.base_uri then
        base_uri = attr.base_uri
        core.log.info("base_uri is overridden: " .. base_uri)
    end

    return {
        {
            methods = { "POST" },
            uri = { base_uri .. "/subscribe" },
            handler = subscribe_handler,
        },
        {
            methods = { "POST" },
            uri = { base_uri .. "/unsubscribe" },
            handler = unsubscribe_handler,
        },
        {
            methods = { "GET" },
            uri = { base_uri .. "/status" },
            handler = status_handler,
        }
    }
end

return _M
