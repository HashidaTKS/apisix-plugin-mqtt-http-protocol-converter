local core = require("apisix.core")
local ngx = ngx
local resty_http = require("resty.http")
local mqtt_to_json = require('tools.mqtt-to-json')
local protocol = require("mqtt.protocol")
local protocol5 = require("mqtt.protocol5")
local protocol4 = require("mqtt.protocol4")
local packet_type = protocol.packet_type
local client_id_packet_id_delimiter = "__"

local schema = {
    type = "object",
    properties = {
        protocol_version = { type = "number" },
        path = { type = "string" }
    },
}

local plugin_name = "mqtt-http"

local _M = {
    version = 0.1,
    priority = 1000,
    name = plugin_name,
    schema = schema,
}

-- https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901031
local mqtt_reason_code = {
    success = 0,
    unspecified_error = 128,
    packet_identifier_in_use = 145,
    packet_identifier_not_found = 146
}

local function create_mqtt_http_packet_dic_key(mqtt_message)
    if not (mqtt_message.type == packet_type.PUBLISH and mqtt_message.qos == 2) and
       not (mqtt_message.type == packet_type.PUBREL) then
        return nil
    end
    local client_id = ""
    if mqtt_message.user_properties then
        if mqtt_message.user_properties.rs_cid then
            client_id = mqtt_message.user_properties.rs_cid
            -- rs_cid (relay-subscriber-client-id) is a special property to support for QoS2 
            -- in relay-subscriber and mqtt-http.
            -- It is used to discern senders and added by relay-subscriber.
            -- It should be removed in order to avoid sending it to HTTP server.
            mqtt_message.user_properties.rs_cid = nil
            if not next(mqtt_message.user_properties) then
                mqtt_message.user_properties = nil
            end
        end
    end

    local packet_id = mqtt_message.packet_id
    local delimiter = client_id_packet_id_delimiter
    return client_id .. delimiter .. packet_id
end

local function send_http(url, mqtt_message, mqtt_http_packet_dic_key)
    if mqtt_message.type == packet_type.PUBLISH and mqtt_message.qos == 2 and mqtt_http_packet_dic_key then
        local packet_dic = ngx.shared.mqtt_http_packet_dic
        local saved_status = packet_dic:get(mqtt_http_packet_dic_key)
        if saved_status then
            -- Already sent message to HTTP server but PUBREC is not reached to client.
            -- Skip to send http and return saved status code
            return { status = saved_status }
        end
    end

    local httpc = resty_http.new()
    local body = mqtt_to_json.convert_mqtt_message_to_json(mqtt_message)
    local res, err = httpc:request_uri(url, {
        method = "POST",
        body = body,
        headers = {
            ["Content-Type"] = "application/json",
        },
        keepalive = false,
    })
    return res, err
end

local function convert_http_status_to_mqtt_reason_code(http_status)
    if http_status >= 200 and http_status < 300 then
        return mqtt_reason_code.success
    end
    return mqtt_reason_code.unspecified_error
end

local function acknowledge_puback(conf, sock, http_response, message)
    -- If connection to http server failed, we don't return PUBACK QoS is 1.
    -- Wait for re-sending of the PUBLISH message from a client.
    if not http_response then
        return
    end
    local make_packet = conf.protocol_version == 5 and protocol5.make_packet or protocol4.make_packet
    local rc = convert_http_status_to_mqtt_reason_code(http_response.status)
    local mqtt_response = make_packet {
        type = packet_type.PUBACK,
        packet_id = message.packet_id,
        rc = rc
    }
    sock:send(mqtt_response)
end

local function acknowledge_pubrec(conf, sock, http_response, message, mqtt_http_packet_dic_key)
    -- If connection to http server failed, we don't return PUBREC even QoS is 2.
    -- Wait for re-sending of the PUBLISH message from a client.
    if not http_response then
        return
    end
    local packet_id = message.packet_id
    local make_packet = conf.protocol_version == 5 and protocol5.make_packet or protocol4.make_packet
    local rc = convert_http_status_to_mqtt_reason_code(http_response.status)
    local packet_dic = ngx.shared.mqtt_http_packet_dic
    if mqtt_http_packet_dic_key then
        if packet_dic:get(mqtt_http_packet_dic_key) and rc == mqtt_reason_code.success then
            rc = mqtt_reason_code.packet_identifier_in_use
        end
        if not packet_dic:get(mqtt_http_packet_dic_key) then
            packet_dic:safe_set(mqtt_http_packet_dic_key, http_response.status)
        end
    end
    local mqtt_response = make_packet {
        type = packet_type.PUBREC,
        packet_id = packet_id,
        rc = rc
    }
    sock:send(mqtt_response)
end

local function acknowledge_pubcomp(conf, sock, message, mqtt_http_packet_dic_key)
    local packet_dic = ngx.shared.mqtt_http_packet_dic
    local rc = 0
    if mqtt_http_packet_dic_key and not packet_dic:get(mqtt_http_packet_dic_key) then
        rc = mqtt_reason_code.packet_identifier_not_found
    end
    local make_packet = conf.protocol_version == 5 and protocol5.make_packet or protocol4.make_packet
    local pubcomp = make_packet {
        type = packet_type.PUBCOMP,
        packet_id = message.packet_id,
        rc = rc
    }
    sock:send(pubcomp)
    if mqtt_http_packet_dic_key then
        packet_dic:delete(mqtt_http_packet_dic_key)
    end
end

local function acknowledge(conf, sock, http_response, message, mqtt_http_packet_dic_key)
    if not message.packet_id then
        -- packet_id MUST be assigned with QoS > 0
        return
    end
    if message.type == packet_type.PUBLISH then
        if message.qos == 1 then
            return acknowledge_puback(conf, sock, http_response, message)
        elseif message.qos == 2 then
            return acknowledge_pubrec(conf, sock, http_response, message, mqtt_http_packet_dic_key)
        end
    elseif message.type == packet_type.PUBREL then
        return acknowledge_pubcomp(conf, sock, message, mqtt_http_packet_dic_key)
    end
end


function _M.check_schema(conf)
    return core.schema.check(schema, conf)
end

function _M.before_proxy(conf, ctx)
    local sock = ngx.req.socket()
    local conn = {
        sock = sock,
    }

    local function receive(size)
        return conn.sock:receive(size)
    end

    local parse_packet = conf.protocol_version == 5 and protocol5.parse_packet or protocol4.parse_packet
    local api_ctx = ngx.ctx.api_ctx
    local picked_server = api_ctx.picked_server
    local host = picked_server.domain or picked_server.host
    local port = picked_server.port
    local target_url = "http://" .. host .. ":" .. port
    if conf.path then
        target_url = target_url .. conf.path
    end

    local failed_to_send_http = false

    local res , err = sock:peek(2)
    if not res then
        core.log.error("Failed to read head two bytes:", err)
        return 500
    end 

    -- Bulk Send Header
    -- * First  byte  : 0xFF (bulk send flag)
    --   * The first byte of MQTT fixed header is never 0xFF at least MQTT v5.0 or earlier.
    --     so we use 0xFF as bulk send flag.
    -- * Second byte :  Message length
    local bulk_send_flag = 0xFF 
    local first_byte, second_byte = string.byte(res, 1, 2)
    core.log.debug("First byte:", first_byte)
    core.log.debug("Second byte:", second_byte)
    local message_length = 1
    if first_byte == bulk_send_flag then
        receive(2) -- get rid of bulk send header
        if second_byte > 0 then
            message_length = second_byte
        end
    end 

    for i = 1, message_length do
        local message, err = parse_packet(receive)
        if not message then
            core.log.error("Failed to parse packet:", err)
            failed_to_send_http = true
            -- Todo: Continue if reading packets is continuable.
            break
        end
        -- Currently, strict QoS2 is supported only when mqtt v5 or later
        local mqtt_http_packet_dic_key = nil
        if conf.protocol_version == 5 then
            mqtt_http_packet_dic_key = create_mqtt_http_packet_dic_key(message)
        end
        local http_response, send_err = send_http(target_url, message, mqtt_http_packet_dic_key)

        if not http_response then
            failed_to_send_http = true
            core.log.error("Failed to request:", send_err)
            core.log.error("Target URL:", target_url)
        end
        acknowledge(conf, sock, http_response, message, mqtt_http_packet_dic_key)
    end

    if failed_to_send_http then
        return 500
    end
    -- Suppress default proxy
    return 200
end

function _M.log(conf, ctx)
end

return _M