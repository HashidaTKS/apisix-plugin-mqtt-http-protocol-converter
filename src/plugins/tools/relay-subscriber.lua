local socket = require("socket")
local unistd = require("posix.unistd")
local mqtt = require("mqtt")
local protocol = require("mqtt.protocol")
local protocol5 = require("mqtt.protocol5")
local protocol4 = require("mqtt.protocol4")
local packet_type = protocol.packet_type
local argparse = require("argparse")
local thread = require('cqueues.thread')
local ffi = require("ffi")
local json = require("cjson.safe")

local parser = argparse("script", "MQTT relay subscriber.")
parser:option("-b --broker", "MQTT broker uri.", "localhost:1883")
parser:option("-U --upstream", "Upstream uri.", "localhost:9100")
parser:option("-p --caller_pid", "Caller pid. Stop this process if caller process stopped", nil)
parser:option("-n --caller_name", "Caller process name. Stop this process if caller process stopped", nil)
parser:option("-v --version", "MQTT protocol version", "5")
parser:option("-u --user", "MQTT protocol username", "stPwSVV73Eqw5LSv0iMXbc4EguS7JyuZR9lxU5uLxI5tiNM8ToTVqNpu85pFtJv9")
parser:option("-P --password", "MQTT protocol password", nil)
parser:option("-t --topic", "MQTT topic name", "luamqtt/#")
parser:option("--cafile", "cafile for ssl", nil)
parser:option("--tls_version", "Tls version for ssl", "tlsv1_2")
parser:option("--log_level", "Specify logging level", "warn")
parser:option("--log_file", "Specify log file path")
parser:option("--log_max_file_size", "Specify max file size (bytes)", 10485760)
parser:option("--log_max_backup_index", "Specify max backup index", 10)
parser:option("--connect_properties", "Specify CONNECT packet properties as JSON", nil)
parser:option("--subscribe_properties", "Specify SUBSCRIBE packet properties as JSON", nil)

local args = parser:parse()

require "logging.rolling_file"
local max_file_size = tonumber(args.log_max_file_size)
local default_log_path = "/usr/local/apisix-plugin-mqtt-http-protocol-converter/logs/relay-subscriber.log"
local max_backup_index = tonumber(args.log_max_backup_index)
local log_file = args.log_file or default_log_path
local logger = logging.rolling_file(log_file, max_file_size, max_backup_index)
local log_levels = {
    ["debug"] = logging.DEBUG,
    ["info"] = logging.INFO,
    ["warn"] = logging.WARN,
    ["error"] = logging.ERROR,
    ["fatal"] = logging.FATAL
}

local log_level = args.log_level and log_levels[args.log_level] or logging.WARN
logger:setLevel(log_level)

logger:info("max file size: " .. tostring(max_file_size))
logger:info("max backup index: " .. tostring(max_backup_index))

-- https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901031
local mqtt_reason_code = {
    success = 0,
    packet_identifier_not_found = 146
}

if ffi.os == "OSX" then
    -- luaposix doesn't support clock_gettime() on macOS even on v10.12 or later
    ffi.cdef[[
        typedef long time_t;
        typedef enum {
            CLOCK_REALTIME = 0,
            CLOCK_MONOTONIC = 6
        } clockid_t;
        struct timespec {
            time_t tv_sec;
            long   tv_nsec;
        };
        int clock_gettime(clockid_t clock_id, struct timespec *tp);
    ]]

    function clock_get_monotonic()
        local t = ffi.new("struct timespec[?]", 1)
        ffi.C.clock_gettime(ffi.C.CLOCK_MONOTONIC, t)
        return {
            tv_sec = tonumber(t[0].tv_sec),
            tv_nsec = tonumber(t[0].tv_nsec)
        }
    end
else
    local posix_time = require("posix.time")
    function clock_get_monotonic()
        return posix_time.clock_gettime(posix_time.CLOCK_MONOTONIC)
    end
end

local function caller_process_checker(thread_pipe, caller_pid, caller_name, log_file, log_level, max_log_file_size, max_log_backup_index)
    local unistd = require("posix.unistd")
    local signal = require("posix.signal")
    local cqueues = require("cqueues")
    require("logging.rolling_file")

    local logger = logging.rolling_file(log_file, max_log_file_size, max_log_backup_index)
    logger:setLevel(log_level)

    -- Notify a parent thread that this thread finished to execute 'require("posix.signal")'
    thread_pipe:write("Finish requiring signal\n")
    thread_pipe:flush()
    -- Wait until the parent thread is ready
    thread_pipe:read("*L")

    if not caller_pid then
        logger:warn("caller_pid is not specified. regard as if caller is alive")
        return
    end

    local function check_if_caller_is_alive()  
        local is_caller_alive = false
        if caller_name then
            local handle = io.popen("ps -p " .. caller_pid .. " -o comm=")
            for line in handle:lines() do
                if string.find(line, caller_name) or string.find(line, "nginx: worker process") then
                   -- NOTE: On Darwin, -o comm= output is different
                   is_caller_alive = true
                end
            end
            if not is_caller_alive then
                logger:warn("caller_name process can't be found: " .. caller_name)
            end
            handle:close()
        else
            local result = os.execute("ps -p " .. caller_pid .. " > /dev/null")
            is_caller_alive = result == 0
        end
        logger:debug("check_if_caller_is_alive:is_caller_alive: " .. tostring(is_caller_alive))
        return is_caller_alive
    end

    while check_if_caller_is_alive() do
        cqueues.sleep(0.5)
    end

    signal.kill(unistd.getpid(), signal.SIGTERM)
end

local function get_tv_diff_as_sec(left, right)
    local sec_diff = left.tv_sec - right.tv_sec
    local nsec_diff = left.tv_nsec - right.tv_nsec
    return sec_diff + nsec_diff / (1000.0 * 1000.0 * 1000.0)
end

local command_name = "mqtt-relay-subscriber"
local command_version = 1
local pid = unistd.getpid()

math.randomseed(os.time())

local secure = false
if args.cafile then
    secure = {
        cafile = args.cafile,
        mode = "client",
        protocol = args.tls_version,
        options = "all",
        verify = "peer"
    }
end

local mqtt_args = {
    uri = args.broker,
    username = args.user,
    password = args.password,
    secure = secure,
    clean = true,
    version = tonumber(args.version),
    id = string.format("%s-v%s-%d-%07x", command_name, command_version, pid ,math.random(1, 0xFFFFFFF))
}

if args.connect_properties then
    local connect_properties = json.decode(args.connect_properties)
    assert(not connect_properties["authentication_method"], "AUTH is not supported yet.")
    assert(not connect_properties["authentication_data"], "AUTH is not supported yet.")
    assert(not connect_properties["receive_maximum"], "Receive Maximum is not supported yet.")
    if connect_properties["user_property"] then
        mqtt_args.user_properties = connect_properties["user_property"]
        connect_properties["user_property"] = nil
    end
    mqtt_args.properties = connect_properties
end

local client = mqtt.client(mqtt_args)
local upstream_host, upstream_port_str = string.match(args.upstream, "^([^%s]+):(%d+)$")
local upstream_port = tonumber(upstream_port_str)
local make_packet = mqtt_args.version == 5 and protocol5.make_packet or protocol4.make_packet
local parse_packet = mqtt_args.version == 5 and protocol5.parse_packet or protocol4.parse_packet

local last_send_time = clock_get_monotonic()

local bulk_message_sender = {
    buffer = {},
    max_buffer_count = 10, -- Must less than 256
    wait_sec = 0.001,
    upstream_host = upstream_host,
    upstream_port = upstream_port,
    acknowledge =
        function(self, message, receive_func)
            if message.qos == 0 or not message.packet_id then
                return true, nil
            end
            local response, err = parse_packet(receive_func)
            if response and response.packet_id == message.packet_id then
                if (message.qos == 1 and response.type == packet_type.PUBACK) or
                   (message.qos == 2 and response.type == packet_type.PUBREC) then
                    return client:acknowledge(message, response.rc)
                else
                    return false, "bulk_message_sender.acknowledge: not expected response format"
                end
            end
            return false, "bulk_message_sender.acknowledge: parse_packet failed: " .. err
        end,
    send_if_need =
        function(self)
            local buffer_count = #self.buffer
            local current_time = clock_get_monotonic()
            local elapsed_sec = get_tv_diff_as_sec(current_time, last_send_time)
            if buffer_count >= self.max_buffer_count or
              (elapsed_sec >= self.wait_sec and buffer_count > 0) then
                self:send()
            end
        end,
    send =
        function(self)
            local buffer_count = #self.buffer
            local relay_client = socket.connect(self.upstream_host, self.upstream_port)
            local function receive(size)
                return relay_client:receive(size)
            end
            -- Bulk Send Header
            -- * First byte  : 0xFF (bulk send flag)
            --   * The first byte of MQTT fixed header is never 0xFF at least MQTT v5.0 or earlier.
            --     so we use 0xFF as bulk send flag.
            -- * Second byte :  Message length
            relay_client:send(string.char(0xff))
            relay_client:send(string.char(buffer_count))
            for i = 1, buffer_count do
                local message = self.buffer[i]
                if message.qos == 2 then
                    if not message.user_properties then
                        message.user_properties = {}
                    end
                    -- rs_cid (relay-subscriber-client-id) is a special property to support for QoS2 
                    -- in relay-subscriber and mqtt-http.
                    -- It is used to discern senders and added by relay-subscriber.
                    message.user_properties["rs_cid"] = mqtt_args.id
                end
                local packet = make_packet(message)
                local data = tostring(packet)
                relay_client:send(data)
                local success_acknowledge, err = self:acknowledge(message, receive)
                if not success_acknowledge then
                    logger:error(err)
                end
            end
            relay_client:close()
            self.buffer = {}
            last_send_time = clock_get_monotonic()
        end,
    append_buffer =
        function(self, message)
            table.insert(self.buffer, message)
        end,
}

local complete_connection = false
-- override class method in order to send PUBREL to HTTP server
client.original_acknowledge_pubcomp = client.acknowledge_pubcomp
client.acknowledge_pubcomp = function(self, packet_id)
    local relay_client = socket.connect(upstream_host, upstream_port)
    relay_client:settimeout(5)

    local packet = {}
    if packet_id then
        packet = make_packet({
            type = packet_type.PUBREL,
            packet_id = packet_id,
            rc = mqtt_reason_code.success,
            user_properties = {
                rs_cid = mqtt_args.id
            }
        })
    else
        packet = make_packet({
            type = packet_type.PUBREL,
            rc = mqtt_reason_code.packet_identifier_not_found,
            user_properties = {
                rs_cid = mqtt_args.id
            }
        })
    end
    relay_client:send(tostring(packet))
    last_send_time = clock_get_monotonic()

    local function receive(size)
        return relay_client:receive(size)
    end

    local pubcomp, err = parse_packet(receive)
    relay_client:close()
    local success_acknowledge = false
    if pubcomp and pubcomp.packet_id == packet_id then
        if pubcomp.type == packet_type.PUBCOMP then
            success_acknowledge = self.original_acknowledge_pubcomp(self, packet_id)
            if not success_acknowledge then
                logger:error("acknowledge_pubcomp: failed to send pubcomp")
            end
        else
            logger:error("acknowledge_pubcomp: not expected pubcomp format")
        end
    else
        logger:error("acknowledge_pubcomp: parse_packet failed: " .. err)
    end

    if success_acknowledge then
        return true
    end

    local conn = self.connection
    if not conn then
        logger:error("acknowledge_pubcomp: connection not opened")
    end
    -- packet_id is deleted from wait_for_pubrel before this function is called
    -- and if packet_id is not found in wait_for_pubrel, this function is not called.
    -- So re-adding this packet_id in order to be able to call this function by the same
    -- packet_id next time.
    if packet_id then
        conn.wait_for_pubrel[packet_id] = true
    end
    return false
end

local first_message = true
local message_count = 0
local logging_message_interval = 10000

client:on{
    connect = function(connack)
        complete_connection = true
        if connack.rc ~= mqtt_reason_code.success then
           logger:error("client:on:connect failed: " .. connack:reason_string())
        end
        assert(connack.rc == mqtt_reason_code.success, "Failed to connect")
        if connack.properties and connack.properties.server_keep_alive then
            client.args.keep_alive = connack.properties.server_keep_alive
        end
        -- Currently, QoS2 is supported only when mqtt v5 or later
        local qos = mqtt_args.version >= 5 and 2 or 1
        local subscribe_args = {
            topic=args.topic,
            qos=qos
        }
        if args.subscribe_properties then
            local subscribe_properties = json.decode(args.subscribe_properties)
            if subscribe_properties["user_property"] then
                subscribe_args.subscribe_properties = subscribe_properties["user_property"]
                subscribe_properties["user_property"] = nil
            end
            if subscribe_properties["subscription_identifier"] then
                -- In luamqtt, subscription_identifier is named as subscription_identifiers
                -- and requires the list-table type.
                local value = subscribe_properties["subscription_identifier"]
                if type(value) == "number" then
                    subscribe_properties["subscription_identifiers"] = { value }
                else
                    subscribe_properties["subscription_identifiers"] = value
                end
                subscribe_properties["subscription_identifier"] = nil
            end
            subscribe_args.properties = subscribe_properties
        end
        assert(client:subscribe(subscribe_args))
        if log_level == logging.DEBUG then
            local current_time = clock_get_monotonic()
            logger:debug("connect to broker:" .. current_time.tv_sec .. "." .. string.format("%09d", current_time.tv_nsec))
        end
    end,

    message = function(msg)
        if log_level == logging.DEBUG then
            message_count = message_count + 1
            if first_message or message_count % logging_message_interval == 0 then
                local current_time = clock_get_monotonic()
                if first_message then
                    logger:debug("receive first message: " .. current_time.tv_sec .. "." .. string.format("%09d", current_time.tv_nsec))
                    first_message = false
                else
                    logger:debug("receive " .. logging_message_interval  .. " messages: " .. current_time.tv_sec .. "." .. string.format("%09d", current_time.tv_nsec))
                    message_count = 0
                end
            end
        end
        bulk_message_sender:append_buffer(msg)
        bulk_message_sender:send_if_need()
    end,

    error = function(err)
        logger:error("client:on:error something weird: " .. err)
    end,
}

local autocreate = true
local warm_sec = 1 -- skip sleeping if elapsed seconds from last_send_time are less than to this value
local loop_options = {
    timeout = 0.005, -- network operations timeout in seconds
    sleep = 0.1,     -- sleep interval after each iteration
    sleep_function = function(time)
        local elapsed_sec_from_last_send = get_tv_diff_as_sec(clock_get_monotonic(), last_send_time)
        if elapsed_sec_from_last_send >= warm_sec then
            socket.sleep(time)
        end
    end
}

local loop = mqtt.get_ioloop(autocreate, loop_options)
loop:add(client)

print(pid)
io.flush()

assert(client:start_connecting())

local started_thread, thread_pipe = thread.start(caller_process_checker, args.caller_pid, args.caller_name, log_file, log_level, max_file_size, max_backup_index)

thread_pipe:read("*L")

-- In order to reset signals set by "require" in caller_process_checker, we should execute 'require ("posix.signal")' at this point.
local signal = require("posix.signal")
local need_to_continue = true
signal.signal(signal.SIGTERM, function()
    logger:info("signal SIGTERM is trapped.")
    need_to_continue = false
end)

thread_pipe:write("Finish setting signal\n")
thread_pipe:flush()

local start_time = clock_get_monotonic()
while need_to_continue do
    loop:iteration()
    bulk_message_sender:send_if_need()
    if not complete_connection then
        local current_time = clock_get_monotonic()
        local elapsed_sec = get_tv_diff_as_sec(current_time, start_time)
        if elapsed_sec > 0.5 then
            -- timeout
            logger:error("Failed to establish connection to the broker: timeout")
            break
        end
    end
end

if complete_connection then
    bulk_message_sender:send()
    client:unsubscribe({ topic = args.topic })
end
client:disconnect()
