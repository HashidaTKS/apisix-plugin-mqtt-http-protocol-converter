#
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Premise: MQTT broker is running on 127.0.0.1:1883

use t::APISIX 'no_plan';
use Cwd qw(cwd);

repeat_each(1);
log_level('debug');
no_long_string();
no_root_location();
no_shuffle();

add_block_preprocessor(sub {
    my ($block) = @_;

    my $apisix_extra_plugin_home = $ENV{APISIX_EXTRA_PLUGIN_HOME} || cwd();

    my $extra_yaml_config = <<_EOC_;   
plugins:
  - public-api                     # priority: 501
  - mqtt-http-subscriber           # priority: 40
plugin_attr:
  mqtt-http-subscriber:
    base_uri: "/apisix/plugin/mqtt-http-subscriber"
    relay_subscriber_path: "${apisix_extra_plugin_home}/tools/relay-subscriber.lua"
_EOC_
    $block->set_value("extra_yaml_config", $extra_yaml_config);

    my $http_config = $block->http_config // <<_EOC_;
    lua_shared_dict mqtt_http_subscriber_pid_dic 1m;
_EOC_

    $block->set_value("http_config", $http_config);

});
run_tests;

__DATA__

=== TEST 1: add plugin
--- config
    location /t {
        content_by_lua_block {
            local t = require("lib.test_admin").test
            local code, body = t('/apisix/admin/routes/1',
                ngx.HTTP_PUT,
                [[{
                    "uri": "/apisix/plugin/mqtt-http-subscriber/unsubscribe",
                    "plugins": {
                        "public-api": {}
                    }
                }]]
            )

            if code >= 300 then
                ngx.status = code
                ngx.say(body)
                return
            end

            code, body = t('/apisix/admin/routes/2',
                ngx.HTTP_PUT,
                [[{
                    "uri": "/apisix/plugin/mqtt-http-subscriber/subscribe",
                    "plugins": {
                        "public-api": {}
                    }
                }]]
            )

            if code >= 300 then
                ngx.status = code
                ngx.say(body)
                return
            end

            code, body = t('/apisix/admin/routes/3',
                ngx.HTTP_PUT,
                [[{
                    "uri": "/apisix/plugin/mqtt-http-subscriber/status",
                    "plugins": {
                        "public-api": {}
                    }
                }]]
            )

            if code >= 300 then
                ngx.status = code
                ngx.say(body)
                return
            end
            ngx.say(body)
        }
    }
--- request
GET /t
--- response_body
passed
--- no_error_log
[error]

=== TEST 2: status: No subscriber
--- request
GET /apisix/plugin/mqtt-http-subscriber/status
--- response_body
No subscriber is running
--- no_error_log
[error]

=== TEST 2: subscribe default except broker_uri
--- request eval
my $host = $ENV{BROKER_SERVER_ADDR} || "localhost";
"POST /apisix/plugin/mqtt-http-subscriber/subscribe
{ \"broker_uri\": \"${host}:1883\" }"
--- more_headers
Content-Type: application/json
--- response_body
Subscribed
--- error_log eval
my $host = $ENV{BROKER_SERVER_ADDR} || "localhost";
[ "broker_uri: ${host}",
   "upstream_uri: localhost:9100",
   "topic: /#",
   "version: 5"
]
--- no_error_log
[error]

=== TEST 3: unsubscribe default except broker_uri
--- request eval
my $host = $ENV{BROKER_SERVER_ADDR} || "localhost";
"POST /apisix/plugin/mqtt-http-subscriber/unsubscribe
{ \"broker_uri\": \"${host}:1883\" }"
--- more_headers
Content-Type: application/json
--- error_log eval
my $host = $ENV{BROKER_SERVER_ADDR} || "localhost";
[ "broker_uri: ${host}",
  "upstream_uri: localhost:9100",
  "topic: /#"
]
--- response_body
Unsubscribed
--- no_error_log
[error]

=== TEST 4: subscribe invalid broker
--- request eval
my $host = $ENV{BROKER_SERVER_ADDR} || "localhost";
"POST /apisix/plugin/mqtt-http-subscriber/subscribe
{ \"broker_uri\": \"${host}:1884\" }"
--- more_headers
Content-Type: application/json
--- response_body eval
qr/Failed to start subscriber.*failed to open network connection/
--- error_code: 500
--- error_log
Subscriber process is terminated
Failed to start subscriber

=== TEST 5: unsubscribe invalid broker (regarded as success)
--- request eval
my $host = $ENV{BROKER_SERVER_ADDR} || "localhost";
"POST /apisix/plugin/mqtt-http-subscriber/unsubscribe
{ \"broker_uri\": \"${host}:1884\" }"
--- more_headers
Content-Type: application/json
--- response_body
Unsubscribed
--- no_error_log
[error]

=== TEST 6: subscribe attr
--- yaml_config
plugin_attr:
  mqtt-http-subscriber:
    base_uri: "/apisix/plugin/mqtt-http-subscriber"
    relay_subscriber_path: "overridden_relay_subscriber_path"
    relay_subscriber_log_level: "debug"
    relay_subscriber_log_max_file_size: "100"
    relay_subscriber_log_max_backup_index: "5"
    cafile: "path/to/cafile"
    tls_version: "tlsv1_2"
--- request eval
my $host = $ENV{BROKER_SERVER_ADDR} || "localhost";
"POST /apisix/plugin/mqtt-http-subscriber/subscribe
{ \"broker_uri\": \"${host}:1883\" }"
--- more_headers
Content-Type: application/json
--- response_body eval
# overridden_relay_subscriber_path is not exists, so command fails
qr/Failed to start subscriber/
--- error_code: 500
--- error_log
relay_subscriber_path is overridden
relay_subscriber_log_level is overridden
relay_subscriber_log_max_file_size is overridden
relay_subscriber_log_max_backup_index is overridden
cafile:
tls_version:
luajit overridden_relay_subscriber_path
log_level debug
log_max_file_size 100
log_max_backup_index 5

=== TEST 7: subscribe specify value
--- request eval
my $host = $ENV{BROKER_SERVER_ADDR} || "localhost";
"POST /apisix/plugin/mqtt-http-subscriber/subscribe
{ \"broker_uri\": \"${host}:1883\", 
  \"upstream_uri\": \"localhost:9999\",
  \"topic\": \"new_topic\",
  \"mqtt_version\": 4,
  \"user_name\": \"user\",
  \"password\" : \"pass\"
}"
--- more_headers
Content-Type: application/json
--- response_body
Subscribed
--- error_log eval
my $host = $ENV{BROKER_SERVER_ADDR} || "localhost";
[ "broker_uri: ${host}",
   "upstream_uri: localhost:9999",
   "topic: new_topic",
   "version: 4",
   "user_name: user",
   "password: pass"
]
--- no_error_log
[error]

=== TEST 8: unsubscribe specify value
--- request eval
my $host = $ENV{BROKER_SERVER_ADDR} || "localhost";
"POST /apisix/plugin/mqtt-http-subscriber/unsubscribe
{ \"broker_uri\": \"${host}:1883\", 
  \"upstream_uri\": \"localhost:9999\",
  \"topic\": \"new_topic\",
  \"mqtt_version\": 4
}"
--- more_headers
Content-Type: application/json
--- response_body
Unsubscribed
--- error_log eval
my $host = $ENV{BROKER_SERVER_ADDR} || "localhost";
[ "broker_uri: ${host}",
   "upstream_uri: localhost:9999",
   "topic: new_topic"
]
--- no_error_log
[error]
