#!/bin/bash

function prompt_help_and_exit () {
    echo "Usage: $0 APISIX_HOME_PATH"
    echo "Premise: * APISIX runs (port: 9080)"
    echo "         * etcd runs (port: 2379)"
    echo "         * mosquitto_pub is installed"
    echo "         * MQTT broker runs (port: 1883)"
    echo "         * HTTP server runs (port: 8001 and 8002)"
    echo "         * This plugin is installed"
    exit 1
}

if [ $# -ne 1 ] || [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
    prompt_help_and_exit
fi

apisix_home=$1

if [ ! -d $apisix_home ]; then
    prompt_help_and_exit
fi


pushd $apisix_home
    if [ $? -ne 0 ]; then
        exit 1
    fi
    apisix stop
    sleep 1
    apisix start
    if [ $? -ne 0 ]; then
        exit 1
    fi
popd

curl http://127.0.0.1:9180/apisix/admin/stream_routes/1 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X DELETE
curl http://127.0.0.1:9180/apisix/admin/stream_routes/2 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X DELETE
curl http://127.0.0.1:9180/apisix/admin/routes/1  -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X DELETE
curl http://127.0.0.1:9180/apisix/admin/routes/2  -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X DELETE
curl http://127.0.0.1:9180/apisix/admin/routes/3  -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X DELETE
curl http://127.0.0.1:9180/apisix/admin/routes/4  -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X DELETE

curl http://127.0.0.1:9180/apisix/admin/stream_routes/1 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "server_addr": "127.0.0.1",
    "server_port": 9100,
    "plugins": {
        "mqtt-http": {
            "protocol_version": 4
        }
    },
    "upstream": {
        "type": "roundrobin",
        "nodes": [
        {
            "host": "127.0.0.1",
            "port": 8001,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8002,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8003,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8004,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8005,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8006,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8007,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8008,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8009,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8010,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8011,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8012,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8013,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8014,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8015,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8016,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8017,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8018,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8019,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8020,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8021,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8022,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8023,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8024,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8025,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8026,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8027,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8028,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8029,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8030,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8031,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8032,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8033,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8034,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8035,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8036,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8037,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8038,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8039,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8040,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8041,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8042,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8043,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8044,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8045,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8046,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8047,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8048,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8049,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8050,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8101,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8102,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8103,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8104,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8105,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8106,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8107,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8108,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8109,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8110,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8111,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8112,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8113,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8114,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8115,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8116,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8117,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8118,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8119,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8120,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8121,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8122,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8123,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8124,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8125,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8126,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8127,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8128,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8129,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8130,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8131,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8132,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8133,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8134,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8135,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8136,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8137,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8138,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8139,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8140,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8141,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8142,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8143,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8144,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8145,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8146,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8147,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8148,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8149,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8150,
            "weight": 1
        }
        ]
    }
}'
curl http://127.0.0.1:9180/apisix/admin/stream_routes/2 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "server_addr": "127.0.0.1",
    "server_port": 9101,
    "plugins": {
        "mqtt-http": {
            "protocol_version": 4
        }
    },
    "upstream": {
        "type": "roundrobin",
        "nodes": [
        {
            "host": "127.0.0.1",
            "port": 8001,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8002,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8003,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8004,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8005,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8006,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8007,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8008,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8009,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8010,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8011,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8012,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8013,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8014,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8015,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8016,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8017,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8018,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8019,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8020,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8021,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8022,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8023,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8024,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8025,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8026,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8027,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8028,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8029,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8030,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8031,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8032,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8033,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8034,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8035,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8036,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8037,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8038,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8039,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8040,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8041,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8042,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8043,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8044,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8045,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8046,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8047,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8048,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8049,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8050,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8101,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8102,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8103,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8104,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8105,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8106,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8107,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8108,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8109,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8110,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8111,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8112,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8113,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8114,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8115,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8116,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8117,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8118,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8119,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8120,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8121,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8122,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8123,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8124,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8125,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8126,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8127,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8128,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8129,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8130,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8131,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8132,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8133,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8134,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8135,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8136,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8137,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8138,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8139,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8140,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8141,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8142,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8143,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8144,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8145,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8146,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8147,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8148,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8149,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8150,
            "weight": 1
        }
        ]
    }
}'
curl -X PUT 'http://127.0.0.1:9180/apisix/admin/routes/2' -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/apisix/plugin/mqtt-http-subscriber/unsubscribe",
    "plugins": {
        "public-api": {}
    }
}'
curl -X PUT 'http://127.0.0.1:9180/apisix/admin/routes/3' -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/apisix/plugin/mqtt-http-subscriber/subscribe",
    "plugins": {
        "public-api": {}
    }
}'
curl -X PUT 'http://127.0.0.1:9180/apisix/admin/routes/4' -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/apisix/plugin/mqtt-http-subscriber/status",
    "plugins": {
        "public-api": {}
    }
}'

# test sample 

# curl -X POST -H "Content-Type: application/json" "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/subscribe" -d '
# {
#     "broker_uri" : "127.0.0.1:1883" ,
#     "upstream_uri" : "127.0.0.1:9101",
#     "topic" : "luamqtt/#"
# }'
# mosquitto_pub -d -h localhost -t luamqtt/test -m "test_message"
# mosquitto_pub -d -h localhost -t luamqtt/test -m "test_message2"
# mosquitto_pub -d -h localhost -t luamqtt/test -m "test_message3"
# mosquitto_pub -d -h localhost -t luamqtt/test -m "test_message4"
# mosquitto_pub -d -h localhost -t luamqtt/test -m "test_message5"
# curl -X GET "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/status"
# curl -X POST -H "Content-Type: application/json" "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/unsubscribe" -d '
# {
#     "broker_uri" : "127.0.0.1:1883" ,
#     "upstream_uri" : "127.0.0.1:9101",
#     "topic" : "luamqtt/#"
# }'
# curl -X GET "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/status"
