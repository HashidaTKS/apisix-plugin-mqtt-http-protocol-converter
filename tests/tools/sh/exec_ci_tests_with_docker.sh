docker-compose down
docker-compose pull --ignore-pull-failures package
docker-compose build package
docker-compose pull --ignore-pull-failures broker
docker-compose build broker
docker-compose pull --ignore-pull-failures package
docker-compose run --rm package /apisix-plugin-mqtt-http-protocol-converter/docker/package/build.sh
docker-compose pull --ignore-pull-failures apisix
docker-compose build apisix
docker-compose pull --ignore-pull-failures test
docker-compose build test
docker-compose run --rm --name test test /apisix-plugin-mqtt-http-protocol-converter/docker/test/functional.sh
docker-compose down
docker-compose run --rm --name test test /apisix-plugin-mqtt-http-protocol-converter/docker/test/plugin.sh
docker-compose down
docker-compose run --rm --name test test /apisix-plugin-mqtt-http-protocol-converter/docker/test/unit.sh
docker-compose down
