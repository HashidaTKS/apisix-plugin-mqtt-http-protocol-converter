local protocol = require("mqtt.protocol")
local packet_type = protocol.packet_type
local protocol5 = require("mqtt.protocol5")
local protocol4 = require("mqtt.protocol4")
local json = require("cjson.safe")


dofile("./src/plugins/tools/mqtt-to-json.lua")

describe("all test", function()
    describe("MQTT_v5", function()
        it("publish", function()
            local properties = {
                response_topic = "res_topic_test",
                subscription_identifiers = { 111 }
            }
            local user_properties = { 
                hoge = "foo",
                hogehoge = "foofoo"
            }
            local message = {
                type = packet_type.PUBLISH,
                topic = "test/topic",
                payload = "test message",
                qos = 1,
                packet_id = 1,
                retain = false,
                dup = false,
                properties = properties,
                user_properties = user_properties,
            }
            -- Check if message is valid
            protocol5.make_packet(message)

            local actual_json_string = convert_mqtt_message_to_json(message)
            local expected_json_string =
                '{ \
                    "packet_type": 3, \
                    "properties": { \
                        "response_topic": "res_topic_test", \
                        "subscription_identifier": [ \
                            111 \
                        ] \
                    }, \
                    "topic_name": "test\\/topic", \
                    "packet_type_name": "PUBLISH", \
                    "packet_identifier": 1, \
                    "payload": "test message", \
                    "dup": false, \
                    "qos": 1, \
                    "user_properties": { \
                        "hogehoge": "foofoo", \
                        "hoge": "foo" \
                    }, \
                    "retain": false \
                }'
            local expected = json.decode(expected_json_string)
            local actual = json.decode(actual_json_string)
            assert.are.same(expected, actual)
        end)
        it("puback", function()
            local properties = {
                reason_string = "Out of memory",
            }
            local user_properties = { 
                hoge = "foo",
                hogehoge = "foofoo"
            }
            local message = {
                type = packet_type.PUBACK,
                rc = 2,
                packet_id = 1,
                properties = properties,
                user_properties = user_properties,
            }
            -- Check if message is valid
            protocol5.make_packet(message)

            local actual_json_string = convert_mqtt_message_to_json(message)
            local expected_json_string =
                '{ \
                    "packet_type": 4, \
                    "packet_type_name": "PUBACK", \
                    "reason_code": 2, \
                    "properties": { \
                        "reason_string": "Out of memory" \
                    }, \
                    "user_properties": { \
                        "hogehoge": "foofoo", \
                        "hoge": "foo" \
                    }, \
                    "packet_identifier": 1 \
                }'
            local expected = json.decode(expected_json_string)
            local actual = json.decode(actual_json_string)
            assert.are.same(expected, actual)
        end)
        it("connect", function()
            local properties_for_will = { 
                message_expiry_interval = 1,
                payload_format_indicator = 1,
            }
            local user_properties_for_will = { 
                hoge_will = "foo_will",
                hogehoge_will = "foofoo_will"
            }
            local will = {
                retain = true,
                qos = 1,
                topic = "test_will_topic",
                payload = "test_will_payload",
                properties = properties_for_will,
                user_properties = user_properties_for_will
            }

            local properties = {
                maximum_packet_size = 10,
            }
            local user_properties = { 
                hoge = "foo",
                hogehoge = "foofoo"
            }
            local message = {
                type = packet_type.CONNECT,
                id = "test_id",
                username = "test_name",
                password = "test_password",
                will = will,
                keep_alive = 1,
                properties = properties,
                user_properties = user_properties,
            }
            -- Check if message is valid
            protocol5.make_packet(message)
            
            local actual_json_string = convert_mqtt_message_to_json(message)
            local expected_json_string = 
                '{ \
                    "packet_type": 1, \
                    "identifier": "test_id", \
                    "properties": { \
                        "maximum_packet_size": 10 \
                    }, \
                    "password": "test_password", \
                    "packet_type_name": "CONNECT", \
                    "user_name": "test_name", \
                    "will": { \
                        "user_properties": { \
                            "hoge_will": "foo_will", \
                            "hogehoge_will": "foofoo_will" \
                        }, \
                        "payload": "test_will_payload", \
                        "qos": 1, \
                        "properties": { \
                            "message_expiry_interval": 1, \
                            "payload_format_indicator": 1 \
                        }, \
                        "topic_name": "test_will_topic", \
                        "retain": true \
                    }, \
                    "user_properties": { \
                        "hogehoge": "foofoo", \
                        "hoge": "foo" \
                    }, \
                    "keep_alive": 1 \
                }'
            
            local expected = json.decode(expected_json_string)
            local actual = json.decode(actual_json_string)
            assert.are.same(expected, actual)
        end)
    end)
end)
