# macOS向け開発環境構築方法

## 前提条件

* macOS Catalina以降
* Intel Mac (Apple Siliconは期待されるhomebrewのパス等の構成が異なるため対象外)
* dockerが利用可能なこと
* Homebrewが利用可能なこと
* Xcode 13.2.1以降が利用可能なこと(openrestyの制約)

## セットアップ手順

* Etcdのセットアップ
* VerneMQのセットアップ
* LuaRocksのセットアップ
* Apache APISIXのセットアップ
* 環境変数の設定
* ディレクトリ権限の変更
* APISIXプラグインのセットアップ
* Mosquittoのインストール
* APISIXの起動と終了

### Etcdのセットアップ

Etcdをインストールするには次のコマンドを実行します。

```
$ brew install etcd
$ brew services start etcd
```

Etcdのサービスが期待通り起動しているかは brew services infoを実行することで確認できます。

```
$ brew services info etcd
etcd (homebrew.mxcl.etcd)
Running: ✔
Loaded: ✔
Schedulable: ✘
User: kenhys
PID: 449
```

### VerneMQのセットアップ

VerneMQはdockerコンテナで動作させます。
公式のdockerコンテナとして利用可能な最新のバージョンは2.13と古く、MQTT v5が使えないため、カスタマイズしたコンテナを使用します。

```
$ docker run --name broker -p 1883:1883 -e DOCKER_VERNEMQ_ACCEPT_EULA=yes -e DOCKER_VERNEMQ_ALLOW_ANONYMOUS=on registry.gitlab.com/apisix-plugin-mqtt-http-protocol-converter/apisix-plugin-mqtt-http-protocol-converter:vernemq
```

なお、開発環境であるため、とくに認証はかけない(`DOCKER_VERNEMQ_ALLOW_ANONYMOUS=on`)ものとします。

## LuaRocksのセットアップ

Luaのモジュールを管理できるように次のコマンドを実行します。

```
$ brew install luarocks
```

依存するLuaのモジュールをLuaRocksで管理できるように、次の内容のconfig-5.1.luaを作成します。
ファイルのパスは、`/usr/local/etc/luarocks/config-5.1.lua`とします。

```
-- LuaRocks configuration

rocks_trees = {
   { name = "user", root = home .. "/.luarocks" };
   { name = "system", root = "/usr/local" };
   { name = "apisix", root = "/usr/local/apisix/deps" };
}
lua_interpreter = "luajit";
variables = {
   LUA_DIR = "/usr/local/opt/openresty/luajit";
   LUA_BINDIR = "/usr/local/opt/openresty/luajit/bin";
}
```

## Apache APISIXのセットアップ

APISIXの依存しているものをインストールするには、次のコマンドを実行します。

```
$ brew install openresty/brew/openresty wget curl git pcre openldap
```

本プラグインは現在APISIX 2.15.xと3.4.xをサポートしています。
現時点での最新は2.15.3と3.4.1です。

APISIX 3.4.1をインストールするには、次のコマンドを実行します。

```
$ wget https://dlcdn.apache.org/apisix/3.4.1/apache-apisix-3.4.1-src.tgz
$ mkdir apisix-3.4.1
$ cd apisix-3.4.1
$ tar -xjvf ../apache-apisix-3.4.1-src.tgz
$ sudo ENV_INST_PREFIX=/usr/local make install
```

2.15.3をインストールする場合は、3.4.1を2.15.3に変更してください。

APISIXは、次の階層にインストールされます。

* /usr/local/share/lua/5.1 APISIXのLuaのモジュール
* /usr/local/apisix APISIXの設定ファイルなど

### ディレクトリの権限の変更

設定ファイルを更新できるようにするために、次のコマンドを実行します。

```
$ sudo chown -R $USER /usr/local/apisix
```

これはapisixを起動するときに、nginx.confが再生成されるため、書き込み権限がないと起動に失敗するためです。

### 環境変数の設定

Luaのモジュールの探索パスを次のように指定します。

```
export PATH=$PATH:/usr/local/opt/openresty/luajit/bin
export LUA_PATH="./?.lua;/usr/local/opt/openresty/luajit/share/luajit-2.1.0-beta3/?.lua;/usr/local/share/lua/5.1/?.lua;/usr/local/share/lua/5.1/?/init.lua;/usr/local/opt/openresty/luajit/share/lua/5.1/?.lua;/usr/local/opt/openresty/luajit/share/lua/5.1/?/init.lua;/usr/local/opt/openresty/lualib/?.lua;/usr/local/apisix/deps/share/lua/5.1/?.lua;/usr/local/apisix/deps/share/lua/5.1/?/init.lua"
export LUA_CPATH="./?.so;/usr/local/lib/lua/5.1/?.so;/usr/local/opt/openresty/luajit/lib/lua/5.1/?.so;/usr/local/lib/lua/5.1/loadall.so;/usr/local/opt/openresty/lualib/?.so;/usr/local/apisix/deps/lib/lua/5.1/?.so"
```

### APISIXプラグインのセットアップ

APISIXプラグインをインストールするには次のコマンドを実行します。

```
$ git clone https://gitlab.com/apisix-plugin-mqtt-http-protocol-converter/apisix-plugin-mqtt-http-protocol-converter.git
$ cd apisix-plugin-mqtt-http-protocol-converter
$ sudo luarocks --lua-dir=/usr/local/opt/openresty/luajit make CRYPTO_DIR=/usr/local/opt/openresty-openssl111 OPENSSL_DIR=/usr/local/opt/openresty-openssl111
```

上記コマンドの実行により、依存関係含めLuaのモジュールがインストールされます。

`sudo luarocks --lua-dir=/usr/local/opt/openresty/luajit list --porcelain`でインストールされたモジュールを確認できます。
各モジュールのバージョンは、実行環境のその時点の最新のバージョンにより異なります。

```
$ sudo luarocks --lua-dir=/usr/local/opt/openresty/luajit list --porcelain
api7-dkjson	0.1.1-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
api7-lua-resty-http	0.2.0-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
api7-lua-tinyyaml	0.4.2-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
apisix-plugin-mqtt-http-protocol-converter	1.0-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
argparse	0.7.1-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
base64	1.5-3	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
binaryheap	0.4-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
cqueues	20200726.51-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
dkjson	2.6-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
ext-plugin-proto	0.5.0-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
graphql	0.0.2-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
inspect	3.1.3-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
jsonschema	0.9.8-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lpeg	1.0.2-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lrandom	20180729-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lrexlib-pcre	2.9.1-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-protobuf	0.3.4-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-balancer	0.04-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-cookie	0.1.0-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-ctxdump	0.1-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-dns-client	6.0.2-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-etcd	1.7.0-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-expr	1.3.0-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-hmac-ffi	0.06-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-http	0.17.0.beta.1-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-ipmatcher	0.6.1-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-jit-uuid	0.0.7-2	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-jwt	0.2.3-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-kafka	0.20-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-logger-socket	2.0.1-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-mediador	0.1.2-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-ngxvar	0.5.2-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-openidc	1.7.5-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-openssl	0.8.8-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-radixtree	2.8.2-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-rocketmq	0.4.1-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-session	3.10-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-template	2.0-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-timer	1.1.0-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-resty-worker-events	2.0.1-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-tinyyaml	1.0-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lua-typeof	0.1-0	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
luafilesystem	1.8.0-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
lualogging	1.6.0-2	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
luamqtt	3.4.2-3	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
luaposix	35.1-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
luasec	1.1.0-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
luasocket	3.0.0-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
luaxxhash	1.0.0-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
net-url	1.1-1	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
penlight	1.12.0-2	installed	/usr/local/apisix/deps/lib/luarocks/rocks-5.1
```

プラグインのモジュール等は次のパスに配置されます。

* `/usr/local/apisix/deps/share/lua/5.1/apisix`
* `/usr/local/apisix/deps/share/lua/5.1/tools`

cloneしたディレクトリに含まれるAPISIXの設定ファイル `package/config-macos-apisix-3.yaml` を `/usr/local/apisix/conf/config.yaml`へと上書きします。
APISIX 2.15.xを使用している場合は、`package/config-macos-apisix-2.yaml`を`/usr/local/apisix/conf/config.yaml`へと上書きしてください。

### Mosquittoのインストール

MQTTのメッセージの送信テストに使うため、次のコマンドでmosquittoをインストールします。

```
$ brew install mosquitto
```

### APISIXの起動と終了

ここまで準備できたら、あとはREADMEの例にならって動作を確認できます。
なお、APISIXの起動と終了は次のコマンドを実行します。

```
$ /usr/local/bin/apisix start
$ /usr/local/bin/apisix stop
```
