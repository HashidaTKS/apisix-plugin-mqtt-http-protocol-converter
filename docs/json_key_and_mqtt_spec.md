# JSON key and corresponding MQTT spec

Basically, only lower case letter and under score is used.
Thus space is converted into underscore (`_`).

This conversion rule will be applied to additional specs in the future.

| JSON key name | Corresponding MQTT spec |
| --- | --- |
| user_name | User Name |
| packet_type | Packet Type |
| packet_type_name | Packet Type Name |
| identifier | Identifier |
| keep_alive | Keep Alive |
| password | Password |
| payload | Payload |
| properties | Properties |
| retain | RETAIN |
| topic_name | Topic Name |
| dup | DUP |
| qos | QoS |
| reason_code | Reason Code |
| packet_identifier | Packet Identifier |
| session_present | Session Present |
| will | Will |
| payload_format_indicator | Payload Format Indicator |
| clean | Clean |
| content_type | Content Type |
| message_expiry_interval | Message Expiry Interval |
| correlation_data | Correlation Data |
| response_topic | Response Topic |
| session_expiry_interval | Session Expiry Interval |
| subscription_identifier | Subscription Identifier |
| server_keep_alive |  Server Keep Alive |
| assigned_client_identifier | Assigned Client Identifier |
| authentication_data | Authentication Data |
| authentication_method | Authentication Method |
| will_delay_interval | Will Delay Interval |
| request_problem_information | Request Problem Information |
| response_information | Response Information |
| request_response_information | Request Response Information |
| reason_string | Reason String |
| server_reference | Server Reference |
| topic_alias_maximum | Topic Alias Maximum |
| receive_maximum | Receive Maximum |
| maximum_qos | Maximum QoS |
| topic_alias | Topic Alias |
| user_property | User Property |
| retain_available | Retain Available |
| wildcard_subscription_available | Wildcard Subscription Available |
| maximum_packet_size | Maximum Packet Size |
| shared_subscription_available | Shared Subscription Available |
| subscription_identifiers_available | Subscription Identifiers Available |
