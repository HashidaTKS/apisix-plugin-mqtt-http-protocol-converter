#!/bin/bash

set -ex

rm -rf /build
cp -a /apisix-plugin-mqtt-http-protocol-converter/ /build
cd /build
export PATH=$PATH:/usr/local/openresty/luajit/bin:/usr/local/apisix-plugin-mqtt-http-protocol-converter/deps/bin/
export LUA_PATH="/usr/local/apisix/deps/share/lua/5.1/?.lua;/usr/local/apisix-plugin-mqtt-http-protocol-converter/deps/share/lua/5.1/?.lua"
export LUA_CPATH="/usr/local/apisix/deps/lib/lua/5.1/?.so;/usr/local/apisix-plugin-mqtt-http-protocol-converter/deps/lib/lua/5.1/?.so"
LANG=C busted tests/mqtt-to-json/mqtt-to-json-test.lua

