#!/bin/bash

set -ex

APISIX_VERSION=${APISIX_VERSION:-3.4.0}
git clone https://github.com/apache/apisix.git
cd apisix
git checkout $APISIX_VERSION
# Prepare toolkit.json
git submodule init && git submodule update

export APISIX_PACKAGE_HOME=/usr/local/apisix 
export APISIX_EXTRA_PLUGIN_HOME=/usr/local/apisix-plugin-mqtt-http-protocol-converter
# Customize APISIX.pm to enable APISIX_PACKAGE_HOME, APISIX_EXTRA_PLUGIN_HOME
/apisix-plugin-mqtt-http-protocol-converter/tests/tools/sh/rewrite-APISIX-pm.sh /apisix
cp -f /apisix-plugin-mqtt-http-protocol-converter/tests/apisix/t/stream-plugin/mqtt-http*.t t/stream-plugin
cp -f /apisix-plugin-mqtt-http-protocol-converter/tests/apisix/t/plugin/mqtt-http-subscriber.t t/plugin

# Disable ldap-auth plugin which cause unexpected loading plugin error.
sed -i'' -e 's/- ldap-auth/#- ldap-auth/' conf/config-default.yaml

# Use MQTT specific configuration
if [[ "${APISIX_VERSION}" < "3" ]]; then
  cp -f /apisix-plugin-mqtt-http-protocol-converter/package/config-mqtt-http-protocol-converter-apisix-2.yaml conf/config.yaml
else
  cp -f /apisix-plugin-mqtt-http-protocol-converter/package/config-mqtt-http-protocol-converter-apisix-3.yaml conf/config.yaml
fi

# Refer external etcd container
dig +short etcd
ETCD_SERVER_ADDR=$(dig +short etcd | tail -n 1)
sed -i'' -e "s,http://127.0.0.1:2379,http://$ETCD_SERVER_ADDR:2379," conf/config-default.yaml

ping -c 1 broker
dig +short broker
BROKER_SERVER_ADDR=$(dig +short broker | tail -n 1)

export LUA_PATH="${LUA_PATH};/apisix/?.lua;/apisix/?/init.lua"
export LUA_CPATH="${LUA_CPATH};/apisix/?.so"

BROKER_SERVER_ADDR=${BROKER_SERVER_ADDR} TEST_NGINX_BINARY=/usr/bin/openresty prove -I. -I./t t/stream-plugin/mqtt-http*.t t/plugin/mqtt-http-subscriber.t
