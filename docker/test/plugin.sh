#!/bin/bash

set -x

APISIX_VERSION=${APISIX_VERSION:-3.4.0}

rm -rf /build
cp -a /apisix-plugin-mqtt-http-protocol-converter/ /build
cd /build

ping -c 1 apisix
ping -c 1 test
ping -c 1 broker
dig +short apisix
APISIX_SERVER_ADDR=$(dig +short apisix | tail -n 1)

dig +short broker
BROKER_SERVER_ADDR=$(dig +short broker | tail -n 1)

dig +short receiver1
V4RECEIVER_ADDR=$(dig +short receiver1 | tail -n 1)

dig +short receiver2
V5RECEIVER_ADDR=$(dig +short receiver2 | tail -n 1)

APISIX_ADMIN_PORT=9180
APISIX_NODE_PORT=9080
SUBSCRIBER_SUBSCRIBE_ENDPOINT=http://${APISIX_SERVER_ADDR}:${APISIX_NODE_PORT}/apisix/plugin/mqtt-http-subscriber/subscribe
SUBSCRIBER_UNSUBSCRIBE_ENDPOINT=http://${APISIX_SERVER_ADDR}:${APISIX_NODE_PORT}/apisix/plugin/mqtt-http-subscriber/unsubscribe
SUBSCRIBER_STATUS_ENDPOINT=http://${APISIX_SERVER_ADDR}:${APISIX_NODE_PORT}/apisix/plugin/mqtt-http-subscriber/status

if [[ "${APISIX_VERSION}" < "3" ]]; then
    ELEMENT_COUNT_KEY=.count
else
    ELEMENT_COUNT_KEY=.total
fi

function assert_response() {
  response=$1
  expected_pattern=$2
  succeed_message=$3
  error_message=$4
  if [[ "$response" =~ "$expected_pattern" ]]; then
    echo $succeed_message
  else
    echo $error_message
    exit 1
  fi
}


function setup_stream_routes
{
    curl http://${APISIX_SERVER_ADDR}:${APISIX_ADMIN_PORT}/apisix/admin/stream_routes/1 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d "
{
    \"server_addr\": \"${APISIX_SERVER_ADDR}\",
    \"server_port\": 9100,
    \"plugins\": {
        \"mqtt-http\": {
            \"protocol_version\": 4
        }
    },
    \"upstream\": {
        \"type\": \"roundrobin\",
        \"nodes\": [{
            \"host\": \"${V4RECEIVER_ADDR}\",
            \"port\": 8001,
            \"weight\": 1
        }]
    }
}"

    curl http://${APISIX_SERVER_ADDR}:${APISIX_ADMIN_PORT}/apisix/admin/stream_routes/2 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d "
{
    \"server_addr\": \"${APISIX_SERVER_ADDR}\",
    \"server_port\": 9101,
    \"plugins\": {
        \"mqtt-http\": {
            \"protocol_version\": 5
        }
    },
    \"upstream\": {
        \"type\": \"roundrobin\",
        \"nodes\": [{
            \"host\": \"${V5RECEIVER_ADDR}\",
            \"port\": 8002,
            \"weight\": 1
        }]
    }
}"
    n_node=$(curl http://${APISIX_SERVER_ADDR}:${APISIX_ADMIN_PORT}/apisix/admin/stream_routes -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X GET | jq "${ELEMENT_COUNT_KEY}")
    assert_response "$n_node" \
                    "2" \
                    "Succeeded to add stream routes" \
                    "Failed to add stream routes"
}

function setup_routes
{
    curl http://${APISIX_SERVER_ADDR}:${APISIX_ADMIN_PORT}/apisix/admin/routes/1 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/apisix/plugin/mqtt-http-subscriber/unsubscribe",
    "plugins": {
        "public-api": {}
    }
}'
    curl http://${APISIX_SERVER_ADDR}:${APISIX_ADMIN_PORT}/apisix/admin/routes/2 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/apisix/plugin/mqtt-http-subscriber/subscribe",
    "plugins": {
        "public-api": {}
    }
}'
    curl http://${APISIX_SERVER_ADDR}:${APISIX_ADMIN_PORT}/apisix/admin/routes/3 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/apisix/plugin/mqtt-http-subscriber/status",
    "plugins": {
        "public-api": {}
    }
}'

    curl http://${APISIX_SERVER_ADDR}:${APISIX_ADMIN_PORT}/apisix/admin/routes/ns -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/apisix/status",
    "plugins": {
        "public-api": {}
    }
}'
    sleep 5

    # routes/1,2,3 and ns
    n_node=$(curl http://${APISIX_SERVER_ADDR}:${APISIX_ADMIN_PORT}/apisix/admin/routes -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X GET | jq "${ELEMENT_COUNT_KEY}")
    assert_response "$n_node" \
                    "4" \
                    "Succeeded to add routes" \
                    "Failed to add routes"

    sleep 5

    status=$(curl ${SUBSCRIBER_STATUS_ENDPOINT})
    assert_response "$status" \
                    "No subscriber is running" \
                    "Succeeded to get subscriber status" \
                    "Failed to get subscriber status"
}

function check_properties
{
    response=$(curl -X POST -H "Content-Type: application/json" ${SUBSCRIBER_SUBSCRIBE_ENDPOINT} -d "
{
    \"broker_uri\" : \"${BROKER_SERVER_ADDR}:1883\",
    \"upstream_uri\" : \"${APISIX_SERVER_ADDR}:9101\",
    \"topic\" : \"luamqtt/#\",
    \"connect_properties\": {
        \"authentication_method\": \"password\"
    }
}")
    assert_response "$response" \
                    "AUTH is not supported yet" \
                    "Succeeded to check authentication_method property" \
                    "Failed to check authentication_method property"

    response=$(curl -X POST -H "Content-Type: application/json" ${SUBSCRIBER_SUBSCRIBE_ENDPOINT} -d "
{
    \"broker_uri\" : \"${BROKER_SERVER_ADDR}:1883\",
    \"upstream_uri\" : \"${APISIX_SERVER_ADDR}:9101\",
    \"topic\" : \"luamqtt/#\",
    \"connect_properties\": {
        \"authentication_data\": \"temp\"
    }
}")
    assert_response "$response" \
                    "AUTH is not supported yet" \
                    "Succeeded to check authentication_data property" \
                    "Failed to check authentication_data property"

    response=$(curl -X POST -H "Content-Type: application/json" ${SUBSCRIBER_SUBSCRIBE_ENDPOINT} -d "
{
    \"broker_uri\" : \"${BROKER_SERVER_ADDR}:1883\",
    \"upstream_uri\" : \"${APISIX_SERVER_ADDR}:9101\",
    \"topic\" : \"luamqtt/#\",
    \"connect_properties\": {
        \"receive_maximum\": 2
    }
}")
    assert_response "$response" \
                    "Receive Maximum is not supported yet" \
                    "Succeeded to check receive_maximum property" \
                    "Failed to check receive_maximum property"

    response=$(curl -X POST -H "Content-Type: application/json" ${SUBSCRIBER_SUBSCRIBE_ENDPOINT} -d "
{
    \"broker_uri\" : \"${BROKER_SERVER_ADDR}:1883\",
    \"upstream_uri\" : \"${APISIX_SERVER_ADDR}:9101\",
    \"topic\" : \"luamqtt/#\",
    \"connect_properties\": {
        \"subscription_identifiers_available\": 2
    }
}")
    assert_response "$response" \
                    "property subscription_identifiers_available is not allowed for packet type 1" \
                    "Succeeded to check an invalid property" \
                    "Failed to check an invalid property"

    response=$(curl -X POST -H "Content-Type: application/json" ${SUBSCRIBER_SUBSCRIBE_ENDPOINT} -d "
{
    \"broker_uri\" : \"${BROKER_SERVER_ADDR}:1883\",
    \"upstream_uri\" : \"${APISIX_SERVER_ADDR}:9101\",
    \"topic\" : \"luamqtt/#\",
    \"subscribe_properties\": {
        \"authentication_method\": \"password\"
    }
}")
    assert_response "$response" \
                    "property authentication_method is not allowed for packet type 8" \
                    "Succeeded to check an invalid property" \
                    "Succeeded to check an invalid property"
}

function setup_subscriber
{
    curl -X POST -H "Content-Type: application/json" ${SUBSCRIBER_SUBSCRIBE_ENDPOINT} -d "
{
    \"broker_uri\" : \"${BROKER_SERVER_ADDR}:1883\",
    \"upstream_uri\" : \"${APISIX_SERVER_ADDR}:9101\",
    \"topic\" : \"luamqtt/#\"
}"

    sleep 5

    curl -X POST -H "Content-Type: application/json" ${SUBSCRIBER_UNSUBSCRIBE_ENDPOINT} -d "
{
    \"broker_uri\" : \"${BROKER_SERVER_ADDR}:1883\",
    \"upstream_uri\" : \"${APISIX_SERVER_ADDR}:9101\",
    \"topic\" : \"luamqtt/#\"
}"

    sleep 5

    status=$(curl ${SUBSCRIBER_STATUS_ENDPOINT})
    if [ "$status" = "No subscriber is running" ]; then
	echo "Succeeded to get subscriber status"
    else
	echo "Failed to get subscriber status"
	exit 1
    fi

    curl -X POST -H "Content-Type: application/json" ${SUBSCRIBER_SUBSCRIBE_ENDPOINT} -d "
{
    \"broker_uri\" : \"${BROKER_SERVER_ADDR}:1883\",
    \"upstream_uri\" : \"${APISIX_SERVER_ADDR}:9101\",
    \"topic\" : \"luamqtt/#\"
}"
}

function post_message
{
    for v in mqttv311 mqttv5; do
	echo "Before posting $v message"
	curl http://${APISIX_SERVER_ADDR}:${APISIX_NODE_PORT}/apisix/status | jq .
	mosquitto_pub --debug --host ${BROKER_SERVER_ADDR} --topic luamqtt/test --message "test $v message" -V $v -q 0
    mosquitto_pub --debug --host ${BROKER_SERVER_ADDR} --topic luamqtt/test --message "test $v message" -V $v -q 1
	sleep 5
	echo "After posting $v message"
	curl http://${APISIX_SERVER_ADDR}:${APISIX_NODE_PORT}/apisix/status | jq .
    done
}

setup_stream_routes
setup_routes
check_properties
setup_subscriber
post_message
