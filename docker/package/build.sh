#!/bin/bash

set -ex

user_id=$(stat --format %u /apisix-plugin-mqtt-http-protocol-converter)
group_id=$(stat --format %g /apisix-plugin-mqtt-http-protocol-converter)

rm -rf /build
cp -a /apisix-plugin-mqtt-http-protocol-converter /build
cd /build
luarocks
rpmdev-setuptree
LANG=C make rpm

mkdir -p /apisix-plugin-mqtt-http-protocol-converter/package/
mv ~/rpmbuild/RPMS/x86_64/apisix-plugin-mqtt-http-protocol-converter-*.rpm /apisix-plugin-mqtt-http-protocol-converter/package/
mv ~/rpmbuild/SRPMS/apisix-plugin-mqtt-http-protocol-converter-*.src.rpm /apisix-plugin-mqtt-http-protocol-converter/package/
(cd /apisix-plugin-mqtt-http-protocol-converter/package/ && sha256sum *.rpm > sha256sum.txt)
chown -R ${user_id}:${group_id} /apisix-plugin-mqtt-http-protocol-converter/package
