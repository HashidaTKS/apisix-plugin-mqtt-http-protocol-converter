# apisix-plugin-mqtt-http-protocol-converter

apisix-plugin-mqtt-http-protocol-converter is a plugin for Apache APISIX which converts MQTT protocol to transfer them as an specific HTTP request.

## Prerequisites

* Apache APISIX 2.15.3 or later
* Etcd 3.4.0 or later
* LuaRocks (optional)
* cqueues/20200726.51-0
* luamqtt 3.4.2-3
* lua-cjson 2.1.0.6-1

Additionally, the following LuaRocks modules are required for testing.

* busted 2.0.0-1
* dkjson 2.6-1
* lua-term 0.7-1
* lua_cliargs 3.0-2
* luafilesystem 1.8.0-1
* luassert 1.8.0-0
* luasystem 0.2.1-0
* mediator_lua 1.1.2-0
* penlight 1.12.0
* say 1.3-1

## Installation

For MIRACLELINUX 8.4, see [Install instruction for MIRACLELINUX 8](docs/miraclelinux8.md) (Written in Japanese)

For other environment, See [Install instructions](docs/install.md)

## Usage

If you already installed all prerequisites components, following steps explain how to use APISIX plugin.

In this example, all prerequisites components are assumed working on the same host.

To make it easy for you to get started with this plugin, here's a list of recommended steps.

* Register `mqtt-http` plugin
* Enable public API
* Subscribe to MQTT broker
* Start HTTP server which works as backends
* Send MQTT message to MQTT broker

In the following example, MQTT messages are sent to MQTT broker, then protocol is converted into HTTP via
APISIX plugin and forwarded into HTTP servers (roundrobin for backend 2 HTTP servers)

### Register `mqtt-http` plugin

Execute the following command to register `mqtt-http` plugin for MQTT protocol version 4.

```
curl http://127.0.0.1:9180/apisix/admin/stream_routes/1 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "server_addr": "127.0.0.1",
    "server_port": 9100,
    "plugins": {
        "mqtt-http": {
            "protocol_version": 4,
            "path": "/path/to/test"
        }
    },
    "upstream": {
        "type": "roundrobin",
        "nodes": [{
            "host": "127.0.0.1",
            "port": 8001,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8002,
            "weight": 1
        }
        ]
    }
}'
```

It means that APISIX accepts MQTT message in port 9100, then forward converted HTTP messages into backend HTTP servers (`http://127.0.0.1:8001/path/to/test`, `http://127.0.0.1:8002/path/to/test` are used)

Execute the following command to register `mqtt-http` plugin for MQTT protocol version 5.

```
curl http://127.0.0.1:9180/apisix/admin/stream_routes/2 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "server_addr": "127.0.0.1",
    "server_port": 9101,
    "plugins": {
        "mqtt-http": {
            "protocol_version": 5,
            "path": "/path/to/test"
        }
    },
    "upstream": {
        "type": "roundrobin",
        "nodes": [{
            "host": "127.0.0.1",
            "port": 8001,
            "weight": 1
        },
        {
            "host": "127.0.0.1",
            "port": 8002,
            "weight": 1
        }
        ]
    }
}'
```

It means that APISIX accepts MQTT message in port 9101, then forward converted HTTP messages into backend HTTP servers (`http://127.0.0.1:8001/path/to/test`, `http://127.0.0.1:8002/path/to/test` are used)


### Enable public API

Execute the following command to register public API.

```
$ curl 'http://127.0.0.1:9180/apisix/admin/routes/1' -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/apisix/plugin/mqtt-http-subscriber/unsubscribe",
    "plugins": {
        "public-api": {}
    }
}'
$ curl 'http://127.0.0.1:9180/apisix/admin/routes/2' -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/apisix/plugin/mqtt-http-subscriber/subscribe",
    "plugins": {
        "public-api": {}
    }
}'
$ curl 'http://127.0.0.1:9180/apisix/admin/routes/3' -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/apisix/plugin/mqtt-http-subscriber/status",
    "plugins": {
        "public-api": {}
    }
}'
```

It means that 3 API are registered - subscribe, unsubscribe, and status.

### Subscribe to MQTT broker

To notify MQTT broker how to connect to APISIX server, execute the following command.

```
$ curl -X POST -H "Content-Type: application/json" "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/subscribe" -d '
{
    "broker_uri" : "127.0.0.1:1883" ,
    "upstream_uri" : "127.0.0.1:9101",
    "topic" : "example/#"
}'
```

You can specify properties of `CONNECT` and `SUBSCRIBE` packets by specifying `connect_properties` and `subscribe_properties`.
The `connect_properties` and `subscribe_properties` should be specified as JSON.

```
$ curl -X POST -H "Content-Type: application/json" "http://127.0.0.1:9080/apisix/plugin/mqtt-http-subscriber/subscribe" -d '
{
    "broker_uri" : "127.0.0.1:1883" ,
    "upstream_uri" : "127.0.0.1:9101",
    "topic" : "example/#",
    "connect_properties": {
        "session_expiry_interval": 10,
        "maximum_packet_size": 1024
    },
    "subscribe_properties": {
        "subscription_identifier": 1,
        "user_property": {
            "bar" : "foo"
        }
    }
}'
```

The properties should be specified with JSON key name in [json_key_and_mqtt_spec.md](docs/json_key_and_mqtt_spec.md).

Please refer to the following links for available properties.

[CONNECT Properties](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901046)
[SUBSCRIBE Properties](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901164)

The following properties are currently not supported.

* authentication_method
  * Because this plugin don't support AUTH packet yet.
* authentication_data
  * Because this plugin don't support AUTH packet yet.
* receive_maximum
  * Because this plugin's receive maximum is always 1 for now.

If above command succeeds, `Subscribed` message is shown on console.

### Start HTTP server which works as backends

Execute the following command to launch backend HTTP servers.

```
$ python3 tests/tools/python3/simple_web_server.py 8001 &
$ python3 tests/tools/python3/simple_web_server.py 8002 &
```

### Send MQTT message to MQTT broker

Execute the following command to send message to MQTT broker.

```
$ mosquitto_pub -h localhost -t example/test -m "test_message"
```

Then, in backend HTTP server side, the following converted message is shown.

```
b'{"type":3,"properties":{},"user_properties":{},"dup":false,"qos":1,"topic":"example\\/test","type_name":"PUBLISH","payload":"test_message","packet_id":1,"retain":false}'        
127.0.0.1 - - [09/Jun/2022 10:46:18] "POST / HTTP/1.1" 200 -
```

## Limitation

### QoS

This plugin support QoS2 only under MQTT version 5.

### Subscribe

#### Properties

As described in [Subscribe to MQTT broker](#subscribe-to-mqtt-broker), you can specify properties of `CONNECT` and `SUBSCRIBE` when subscribing.

However, the following properties are currently not supported.

* authentication_method
  * Because this plugin don't support AUTH packet yet.
* authentication_data
  * Because this plugin don't support AUTH packet yet.
* receive_maximum
  * Because this plugin's receive maximum is always 1 for now.

## Appendix

### MQTT conversion rules

To know how MQTT data are converted into JSON key name, see [JSON key and MQTT spec](docs/json_key_and_mqtt_spec.md)

### MIRACLELINUX 8.4

To install APISIX plugin for MIRACLELINUX 8.4, see [Install instruction for MIRACLELINUX 8](docs/miraclelinux8.md) (Written in Japanese)


## License

[Apache 2.0 License](https://gitlab.com/apisix-plugin-mqtt-http-protocol-converter/apisix-plugin-mqtt-http-protocol-converter/LICENSE)

