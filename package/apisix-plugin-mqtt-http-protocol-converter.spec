%global debug_package %{nil}
%define _apisixdir %{_exec_prefix}/local/%{name}

%if 0%{?rhel} == 8
# https://pagure.io/packaging-committee/issue/738
%define __brp_mangle_shebangs /usr/bin/true
%endif

Name: apisix-plugin-mqtt-http-protocol-converter
Version: 1.2.1
Release: 1%{?dist}
Summary: APISIX plugin which convert MQTT to HTTP
Group: System Environment/Daemons
License: Apache-2.0
URL: https://gitlab.com/apisix-plugin-mqtt-http-protocol-converter/apisix-plugin-mqtt-http-protocol-converter
Source: %{name}-%{version}.tar.gz
BuildArch: x86_64

Requires: apisix >= 2.15.3
Requires: apisix-base >= 1.21.4.1.9
Requires: (%{name}-config-apisix-3 or %{name}-config-apisix-2)
BuildRequires: openresty-openssl111-devel
%{systemd_requires}

%description
APISIX plugin which convert MQTT to HTTP.  APISIX plugin behaves like
a MQTT subscriber, then convert received MQTT to specific HTTP
requests.

%package config-apisix-2
Summary: Config files for %{name}-%{version} working with APISIX 2
Provides: %{name}-config
Conflicts: %{name}-config-apisix-3

%description config-apisix-2
Config files for %{name}-%{version} working with APISIX 2

%package config-apisix-3
Summary: Config files for %{name}-%{version} working with APISIX 3
Provides: %{name}-config
Conflicts: %{name}-config-apisix-2

%description config-apisix-3
Config files for %{name}-%{version} working with APISIX 3

%prep
%setup

%build

%install
rm -rf %{buildroot}
PREFIX=%{_prefix} EXEC_PREFIX=%{_exec_prefix} ROOT_PATH=%{buildroot} SYSCONF_DIR=%{_sysconfdir} APISIX_DIR=%{_apisixdir} make install

%pre
%post
%systemd_post apisix.service

%preun
%systemd_preun apisix.service

%postun
%systemd_postun_with_restart apisix.service

%files
%defattr(-,root,root,-)
%doc %{_prefix}/local/share/%{name}/README.md
%attr(755,nobody,root) %{_apisixdir}/logs
%dir %{_apisixdir}/logs
%config %{_sysconfdir}/sysconfig/%{name}
%{_apisixdir}/apisix/stream/plugins/mqtt-http.lua
%{_apisixdir}/apisix/plugins/mqtt-http-subscriber.lua
%{_apisixdir}/deps/*
%{_apisixdir}/tools/*
%{_apisixdir}/tests/*
%{_apisixdir}/version

%files config-apisix-2
%defattr(-,root,root,-)
%config %{_prefix}/local/apisix/conf/config-mqtt-http-protocol-converter-apisix-2.yaml
%config %{_prefix}/local/apisix/conf/debug-mqtt-http-protocol-converter-apisix-2.yaml
%config %{_sysconfdir}/systemd/system/apisix.service.d/override-apisix-2.conf

%files config-apisix-3
%defattr(-,root,root,-)
%config %{_prefix}/local/apisix/conf/config-mqtt-http-protocol-converter-apisix-3.yaml
%config %{_prefix}/local/apisix/conf/debug-mqtt-http-protocol-converter-apisix-3.yaml
%config %{_sysconfdir}/systemd/system/apisix.service.d/override-apisix-3.conf

%changelog
* Mon Aug 29 2023 Takashi Hashida <hashida@clear-code.com> - 1.2.0-1
- New upstream release
- Submoduled each configs for APISIX 2 and APISIX 3

* Mon Jul 25 2022 Kentaro Hayashi <hayashi@clear-code.com> - 1.1.0-1
- New upstream release

* Thu Jul 7 2022 Kentaro Hayashi <hayashi@clear-code.com> - 1.0.1-1
- Fixed a bug which has QoS1 performance regression

* Thu Jul 7 2022 Kentaro Hayashi <hayashi@clear-code.com> - 1.0.0-1
- initial release
